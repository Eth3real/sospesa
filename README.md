<br />
<div align="center">
  <a href="https://gitlab.com/Eth3real/sospesa">
    <img src="https://challengepost-s3-challengepost.netdna-ssl.com/photos/production/software_photos/001/028/426/datas/gallery.jpg" alt="Logo" width="150" height="150" style="border-radius:16%">
  </a>

  <h3 align="center">SoSpesa - The Ultimate SwiftUI Sample App</h3>



  <div align="center">
    <i>Team Rise</i> submission for the GlobalHack online hackathon.
   <strong>Top 10 finalist project </strong> <br/>for Empowerment & Solidarity Track
    <br />
    <br />
    <a href="https://devpost.com/software/rise-0wfky3"><strong>Find more on Devpost</strong></a>
    <br />
    <br />
    
  </div>
</div>


## Table of Contents

* [About the Project](#about-the-project)
  * [Sample App Idea](#sample-app-idea)
  * [Promo Video](#promo-video)
* [Roadmap](#the-roadmap)
* [Team](#the-team)
* [License](#license)
* [Contacts](#contacts)


## About The Project
<div align="center">
<img src="https://i.ibb.co/KmjswFC/mockupimage-Tavola-disegno-1.png" alt="Logo">
</div>
Here I'm going to cover only the development part, to know more about the idea behind the app go on Devpost, or watch the video.
<br/>
<br/>
The project itself started as 24h hackathon submission, for the online GlobalHack which took place during the pandemic period of the Covid-19. It was required a working prototype in order to be eligible to be selected for the finals.
<br/>
<br/>
Just two coders and less than 12h to the deadline when we started coding. We worked really hard during the night to create something that not only could be functional, but that also looked like the the hi-fi prototypes that the designers gave us.
<br/>
<br/>
What did save us from crying all the night over some constraints and not being able to create any custom UI element in time, like would often happen in those cases?
<br/>
<br/>
The answer: <strong>SwiftUI</strong>


### Sample App Idea
As you may have guessed SwiftUI has allowed us to go lightyears faster during the hackathon coding night, so we thought about keep on working incrementally at the project to give a nice starting point sample app for all those people who would like to approach for the first time this framework, which might appear intimidating at beginning, but it will speed up all the development process as soon as you will start understanding it.

The idea is to add as many feature as we can during the time, such as:

- Camera Management
- CoreData / CloudKit
- MapKit
- Animations
- Custom and reusable UI elements

etc.

We will also try to keep the codebase update as soon as SwiftUI changes and some functionalities will be deprecated or modified.

### Promo Video

[![IMAGE ALT TEXT](http://img.youtube.com/vi/ldxB1L7qXKM/0.jpg)](http://www.youtube.com/watch?v=ldxB1L7qXKM "SoSpesa")


## The Roadmap

<strong>Short-term</strong> milestones:

- Map view customization
- Lottie animations
- Improve and customize more UI elements

<strong>Long-term</strong>  milestones:
- CoreData
- CloudKit
- CoreML models integration
- Video
- Audio
- SpriteKit


## The Team

<strong>Project Managers:</strong>

- Simone Serra Cassano
- Maria Ester Sgambato
- Francesca Franco

 <strong>Developers:</strong>
- Andrea Cascella
- Simone Formisano

 <strong>UI/UX Designers:</strong>
 
- Francesco Arena
- Carmine Acierno
- Simona Palumbo
- Umberto Ciavattone


## License

Distributed under the MIT License. See `LICENSE` for more information.



## Contacts

- Simone Formisano - <a href="https://www.linkedin.com/in/simone-formisano-6057471a8/"><img src="https://is3-ssl.mzstatic.com/image/thumb/Purple123/v4/83/3f/a9/833fa9b8-2b53-9115-30ee-a5394b9a5026/AppIcon-0-0-1x_U007emarketing-0-0-0-6-0-0-sRGB-0-0-0-GLES2_U002c0-512MB-85-220-0-0.png/246x0w.png" alt="Logo" width="15" height="15" style="border-radius:16%"></a>

- Andrea Cascella - <a href="https://www.linkedin.com/in/andrea-cascella-0225461a4/"><img src="https://is3-ssl.mzstatic.com/image/thumb/Purple123/v4/83/3f/a9/833fa9b8-2b53-9115-30ee-a5394b9a5026/AppIcon-0-0-1x_U007emarketing-0-0-0-6-0-0-sRGB-0-0-0-GLES2_U002c0-512MB-85-220-0-0.png/246x0w.png" alt="Logo" width="15" height="15" style="border-radius:16%"></a>

