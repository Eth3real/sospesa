//
//  List.swift
//  RiseApp-SwiftUI
//
//  Created by Andrea Cascella on 11/04/2020.
//  Copyright © 2020 Simone Formisano. All rights reserved.
//

import Foundation

class SpesaList: Identifiable {
    let id: Int
    let elements: [String]
    let quantity: Int
    let place: String
    let total: Int
    
    init(id: Int, elements: [String], place: String, total: Int) {
        self.id = id
        self.elements = elements
        self.quantity = elements.count
        self.place = place
        self.total = total
    }
    
}
