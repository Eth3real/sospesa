//
//  Shop.swift
//  RiseApp-SwiftUI
//
//  Created by Simone Formisano on 10/04/2020.
//  Copyright © 2020 Simone Formisano. All rights reserved.
//

import SwiftUI

class Shop: Identifiable {
    let id: Int
    let image: UIImage
    let name: String
    let address: String
    let inventory: [String]
    
    init(id: Int, image: UIImage, name: String, address: String, inventory: [String]) {
        self.id = id
        self.image = image
        self.name = name
        self.address = address
        self.inventory = inventory
    }
}
