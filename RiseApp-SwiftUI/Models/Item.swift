//
//  Item.swift
//  RiseApp-SwiftUI
//
//  Created by Simone Formisano on 10/04/2020.
//  Copyright © 2020 Simone Formisano. All rights reserved.
//

import SwiftUI

class Item: Identifiable,ObservableObject {
    let id: UUID
    let image: Image
    let name: String
    let categories: [String]
    let shop: Shop // Il negozio in cui si trova lo specifico item
    
    
    @Published var items: [Item] = [Item]()
    
    init(id: UUID, image: Image, name: String, categories: [String], shop: Shop) {
        self.id = id
        self.image = image
        self.name = name
        self.categories = categories
        self.shop = shop
    }
}
