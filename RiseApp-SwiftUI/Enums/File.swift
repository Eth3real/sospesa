//
//  File.swift
//  RiseApp-SwiftUI
//
//  Created by Andrea Cascella on 10/04/2020.
//  Copyright © 2020 Simone Formisano. All rights reserved.
//

import UIKit
import Foundation

enum RiseColor {
    case front
    case back
    
    var color: UIColor {
        switch self {
        case .front:
        return UIColor(red:0.97, green:0.65, blue:0.00, alpha:1.00)
        case .back:
        return UIColor(red:0.93, green:0.93, blue:0.93, alpha:1.00)
        }
    }
    
    init?(color: String) {
        switch color {
        case "front":
            self = .front
        case "back":
            self = .back
        default:
            return nil
        }
    }
    
}
