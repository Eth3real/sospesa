//
//  Category.swift
//  RiseApp-SwiftUI
//
//  Created by Simone Formisano on 10/04/2020.
//  Copyright © 2020 Simone Formisano. All rights reserved.
//

import Foundation

enum Category {
    static let sanitizer = "Sanitizer"
    static let paper = "Paper"
    static let detergent = "Detergent"
    static let food = "Food"
    static let other = "Other"
}

