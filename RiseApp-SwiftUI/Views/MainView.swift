//
//  ContentView.swift
//  customTableViewSwiftUITest
//
//  Created by Simone Formisano on 14/03/2020.
//  Copyright © 2020 Simone Formisano. All rights reserved.
//

import SwiftUI

struct MainView: View {
    @State private var isFirstStart: Bool = true
    @State private var show_modal: Bool = false
    @ObservedObject private var itemTm = Item(id: UUID(), image: Image(uiImage: UIImage(named: "wood")!), name: "", categories: [""], shop: Shop(id: 0, image: UIImage(named: "wood")!, name: "", address: "", inventory: [""]))

    var body: some View {
        NavigationView {
            GeometryReader { geometry in
                VStack {
                    ScrollView(.horizontal, showsIndicators: false) {
                        HStack {
                            CategoryToggleButton(category: "Sanitizer")
                            CategoryToggleButton(category: "Paper")
                            CategoryToggleButton(category: "Food")
                            CategoryToggleButton(category: "Other")
                        }
                    }
                    .padding([.top,.leading], 14)
                    .frame(width: geometry.size.width, height: 45, alignment: .leading)
                    List {
                        ForEach(self.itemTm.items) { item in
                            ItemRow(item: item)
                        }
                        .onDelete(perform: self.delete)
                        .animation(.easeInOut(duration: 1.0))
                        
                        if self.itemTm.items.isEmpty {
                            Spacer()
                            Text("Tap on + button to add a new item")
                            Spacer()
                        }
                    }
                    .onAppear(perform: {
                        UITableView.appearance().separatorStyle = .none
                        if (self.isFirstStart) {
                            self.itemTm.items = [
                                Item(id: UUID(), image: Image(uiImage: UIImage(named: "photoSanitizer")!), name: "Hand Sanitizer 300ml", categories: [Category.detergent], shop: Shop(id: 0, image: UIImage.init(named: "p0")!, name: "Simply", address: "Parco della Vittoria, 21", inventory: [""])),
                                Item(id: UUID(), image: Image(uiImage: UIImage(named: "paper")!), name: "Paper IGP", categories: [Category.paper], shop: Shop(id: 0, image: UIImage.init(named: "p0")!, name: "Primark", address: "Oxford Street", inventory: [""]))
                            ]
                        }
                        self.isFirstStart = false
                    })
                    .navigationBarTitle(Text("Items"), displayMode: .inline)
                    .navigationBarItems(
                        trailing: Button(action: {
                            self.show_modal = true
                        }, label: {
                            ZStack {
                                Image(systemName: "plus")
                                    .foregroundColor(Color(RiseColor.init(color: "front")!.color))
                                    .frame(width: 30, height: 25)
                            }
                        })
                    )
                    .sheet(isPresented: self.$show_modal) {
                        AddItemView(itemTm: self.itemTm)
                    }
                }
            }
        } // navigation view
    }
    
    func delete(at offsets: IndexSet) {
        itemTm.items.remove(atOffsets: offsets)
    }
}

struct MainView_previews: PreviewProvider {
    static var previews: some View {
        MainView()
    }
}

struct CategoryToggleButton: View {
    let category: String
    var body: some View {
        ZStack {
            RoundedRectangle(cornerRadius: 15)
                .stroke(Color(RiseColor(color: "front")!.color), lineWidth: 2)
                .frame(width: 100, height: 30, alignment: .leading)
            Text(category)
        }
        .padding(4)
    }
}


