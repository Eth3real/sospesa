//
//  UserImage.swift
//  customTableViewSwiftUITest
//
//  Created by Simone Formisano on 16/03/2020.
//  Copyright © 2020 Simone Formisano. All rights reserved.
//

import SwiftUI

struct CircularItemImage: View {
    let proPic: Image
    let size: CGFloat
    
    let borderWidth: CGFloat = 4.0
    let borderColor: Color = Color(red: 31/255, green: 173/255, blue: 169/255)
    let shadowColor: Color = Color(red: 0, green: 0, blue: 0, opacity: 0.5)
    
    var body: some View {
        proPic
            .resizable()
            .scaledToFill()
            .frame(width: size, height: size)
            .clipShape(Circle())
    }
}

struct UserImage_Previews: PreviewProvider {
    static var previews: some View {
        Group {
            CircularItemImage(proPic: Image(uiImage:UIImage(named: "photoBread")!), size: 80.0)
            .previewLayout(.fixed(width: 130, height: 130))
        }
    }
}
