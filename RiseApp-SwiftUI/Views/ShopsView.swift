//
//  ShopsView.swift
//  RiseApp-SwiftUI
//
//  Created by Andrea Cascella on 11/04/2020.
//  Copyright © 2020 Simone Formisano. All rights reserved.
//

import SwiftUI

struct ShopsView: View {
    var body: some View {
        NavigationView {
            VStack{
                ScrollView(.horizontal, showsIndicators: false) {
                    HStack {
                        CategoryToggleButton(category: "Sanitizer")
                        CategoryToggleButton(category: "Paper")
                        CategoryToggleButton(category: "Food")
                        CategoryToggleButton(category: "Other")
                    }
                }
                .padding()
                MapView()
                    .edgesIgnoringSafeArea(.all)
            }
            .navigationBarTitle("Shops", displayMode: .inline)
        }
    }
}

struct ShopsView_Previews: PreviewProvider {
    static var previews: some View {
        ShopsView()
    }
}
