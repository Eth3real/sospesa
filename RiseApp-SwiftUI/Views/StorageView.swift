//
//  StorageView.swift
//  RiseApp-SwiftUI
//
//  Created by Andrea Cascella on 11/04/2020.
//  Copyright © 2020 Simone Formisano. All rights reserved.
//

import SwiftUI

struct StorageView: View {
    @State private var i: Double = 60
    
    var items: [Item] = [
        Item(id: UUID(), image: Image(uiImage:UIImage(named: "photoSanitizer")!),name: "Hand Sanitizer", categories: ["sanitizer"], shop: Shop(id: 0, image: UIImage.init(named: "p0")!, name: "Simply", address: "Via dell'Academy", inventory: [""])),
        Item(id: UUID(), image: Image(uiImage:UIImage(named: "photoBread")!),name: "Bread", categories: ["groceries"], shop: Shop(id: 0, image: UIImage.init(named: "p0")!, name: "Simply", address: "Via dell'Academy", inventory: [""])),
        Item(id: UUID(), image: Image(uiImage:UIImage(named: "photoOranges")!),name: "Oranges", categories: ["groceries"], shop: Shop(id: 0, image: UIImage.init(named: "p0")!, name: "Simply", address: "Via dell'Academy", inventory: [""])),
        Item(id: UUID(), image: Image(uiImage:UIImage(named: "paper")!),name: "Paper", categories: ["paper"], shop: Shop(id: 0, image: UIImage.init(named: "p0")!, name: "Simply", address: "Via dell'Academy", inventory: [""])),
    ]
    
    var body: some View {
        NavigationView{
            List{
                ForEach(items){item in
                    StorageItemRow(quantity: Int.random(in: 0...4), item: item)
                }
            }
            .navigationBarItems(
                trailing:
                Image(systemName: "plus")
                    .foregroundColor(Color(RiseColor(color: "front")!.color))
                    .onTapGesture {
                        self.i = Double.random(in: -360...360)
                    }
                )
            .navigationBarTitle("Storage", displayMode: .inline)
        }
    }
}

struct StorageView_Previews: PreviewProvider {
    static var previews: some View {
        StorageView()
    }
}
