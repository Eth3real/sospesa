//
//  AddItemView.swift
//  RiseApp-SwiftUI
//
//  Created by Andrea Cascella on 10/04/2020.
//  Copyright © 2020 Simone Formisano. All rights reserved.
//

import SwiftUI

struct AddItemView: View {
    
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode> // For disposing the view when "Cancel"/"Add" is tapped
    @State var image: Image? = nil
    @State var showCaptureImageView: Bool = false
    @State var buttonClicked = false
    @State private var name: String = ""
    @State private var shop: String = ""
    @State private var comment: String = ""
    @ObservedObject var itemTm: Item
    
    var body: some View {
        NavigationView {
            VStack(alignment: .leading, spacing: 5) {
                TextField("Item name", text: $name).textFieldStyle(RoundedBorderTextFieldStyle())
                    .padding([.top], 40)
                    .padding([.leading, .trailing])
                TextField("Shop name", text: $shop)
                    .textFieldStyle(RoundedBorderTextFieldStyle())
                    .padding()
                HStack {
                    Image("pin")
                        .resizable()
                        .scaledToFit()
                        .frame(width: 32, height: 32)
                    Text("Current shop location")
                }
                .padding()
                HStack {
                      Button(action: {
                         self.showCaptureImageView.toggle()
                       }) {
                            ZStack {
                                Circle()
                                    .fill(Color(red: 0, green: 0, blue: 0, opacity: 0.1))
                                    .frame(width: 128, height: 128)
                                image?
                                    .resizable()
                                    .scaledToFill()
                                    .frame(width: 128, height: 128)
                                    .clipShape(Circle())
                                VStack(alignment: .center){
                                    Image("Group 2")
                                        .resizable()
                                        .frame(maxWidth: 64, maxHeight: 64)
                                        .scaledToFit()
                                }
                            }
                       }
                      .buttonStyle(PlainButtonStyle())
                      .sheet(isPresented: $showCaptureImageView, content: {
                            CaptureImageView(isShown: self.$showCaptureImageView, image: self.$image)
                        })
                        TextView(text: $comment)
                            .background(Color.white)
                            .overlay(
                        RoundedRectangle(cornerRadius: 15)
                            .stroke(lineWidth: 1))
                            .frame(minWidth: 128, idealWidth: 128, maxWidth: 300, minHeight: 128, idealHeight: 128, maxHeight: 128)
                            .textFieldStyle(RoundedBorderTextFieldStyle())
                            .foregroundColor(.gray)
                }
                .padding()
                VStack(alignment: .leading) {
                    Divider()
                        .padding(20)
                    Text("Product tag")
                    ScrollView(.horizontal, showsIndicators: false) {
                        HStack{
                            CategoryToggleIcon(category: "sanitizer", size: 96)
                            CategoryToggleIcon(category: "meat", size: 96)
                            CategoryToggleIcon(category: "paper", size: 96)
                            CategoryToggleIcon(category: "groceries", size: 96)
                        }
                    }
                }
                .scaledToFit()
                .padding()
                Spacer()
            }
            .navigationBarItems(
                leading: Button(action: {
                    // CANCEL BUTTON
                    self.presentationMode.wrappedValue.dismiss()
                    }, label: {
                        Text("Cancel").foregroundColor(Color(RiseColor.init(color: "front")!.color))
                    }),
                trailing: Button(action: {
                    // ADD BUTTON
                    self.itemTm.items.insert(Item(id: UUID(), image: self.image ?? Image(uiImage: UIImage(named: "noImage")!), name: self.name, categories: ["Food"], shop: Shop(id: 0, image: UIImage(named: "wood")!, name: "", address: "Street 87 Evn.", inventory: ["c","x"])), at: 0)
                    self.presentationMode.wrappedValue.dismiss()
                    }, label: {
                        Text("Add").foregroundColor(Color(RiseColor.init(color: "front")!.color))
                    })
            )
            .navigationBarTitle("Add an item", displayMode: .inline)
        }
    }
        
     struct CaptureImageView: UIViewControllerRepresentable {
         @Binding var isShown: Bool
         @Binding var image: Image?
         
         func makeCoordinator() -> Coordinator {
           return Coordinator(isShown: $isShown, image: $image)
         }
        
        func makeUIViewController(context: UIViewControllerRepresentableContext<CaptureImageView>) -> UIImagePickerController {
            let picker = UIImagePickerController()
            picker.sourceType = .camera
            picker.delegate = context.coordinator
            return picker
        }
        
        func updateUIViewController(_ uiViewController: UIImagePickerController,
                                    context: UIViewControllerRepresentableContext<CaptureImageView>) {
            
        }
     }
    
    struct AddItemView_Previews: PreviewProvider {
        static var previews: some View {
            AddItemView(itemTm: Item(id: UUID(), image: Image(uiImage:UIImage(named: "photoSanitizer")!),name: "lollo", categories: ["test category"], shop: Shop(id: 0, image: UIImage.init(named: "p0")!, name: "Simply", address: "Via dell'Academy", inventory: [""])))
        }
    }
    
    struct TextView: UIViewRepresentable {
        @Binding var text: String
        
        func makeCoordinator() -> Coordinator {
            Coordinator(self)
        }
        
        func makeUIView(context: Context) -> UITextView {
            
            let myTextView = UITextView()
            myTextView.text = "Optional Comments"
            myTextView.delegate = context.coordinator
            
            myTextView.font = UIFont(name: "HelveticaNeue", size: 15)
            myTextView.isScrollEnabled = true
            myTextView.isEditable = true
            myTextView.isUserInteractionEnabled = true
            myTextView.backgroundColor = UIColor(white: 0, alpha: 0)
            myTextView.layer.cornerRadius = 20
            return myTextView
        }
        
        func updateUIView(_ uiView: UITextView, context: Context) {
            uiView.text = text
        }
        
        class Coordinator : NSObject, UITextViewDelegate {
            
            var parent: TextView
            
            init(_ uiTextView: TextView) {
                self.parent = uiTextView
            }
            
            func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
                return true
            }
            
            func textViewDidChange(_ textView: UITextView) {
                print("text now: \(String(describing: textView.text!))")
                self.parent.text = textView.text
            }
        }
    }
    
    struct CategoryToggleIcon: View {
        
        @State var isSelected: Bool = false
        
        var category: String = ""
        var size: CGFloat
        let shadowColor: Color = Color(red: 0, green: 0, blue: 0, opacity: 0.1)
        
        
        var body: some View{
            ZStack{
                RoundedRectangle(cornerRadius: 20).fill(isSelected == true ? Color(RiseColor(color: "front")!.color) : Color(red: 255, green: 255, blue: 255, opacity: 1))
                    .frame(width: 85, height: 85)
                    .shadow(color: Color(red: 0, green: 0, blue: 0, opacity: 0.3), radius: 6)
                
                VStack {
                    Spacer()
                    Image(returnImage(category: category))
                        .colorMultiply(isSelected == true ? .white : .black)
                        .scaleEffect(0.7)
                        .onTapGesture {
                            self.isSelected.toggle()
                    }
                    Spacer()
                    Text(category)
                        .offset(CGSize(width: 0, height: -20))
                }
            }
            .padding(2.3)
        }
        
        func returnImage(category: String) -> String{
            switch category {
            case "paper" : return "iconPaper"
            case "sanitizer" : return "iconSanitizer"
            case "meat" : return "iconMeat"
            case "groceries" : return "iconGroceries"
            default:
                return "iconPaper"
            }
        }
    }
}
