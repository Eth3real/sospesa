//
//  AddPersonVIew.swift
//  customTableViewSwiftUITest
//
//  Created by Simone Formisano on 15/03/2020.
//  Copyright © 2020 Simone Formisano. All rights reserved.
//

import SwiftUI

struct AddPersonView: View {
    let passedPerson: Person
    var body: some View {
        Text("Hello \(passedPerson.name)")
    }
}
