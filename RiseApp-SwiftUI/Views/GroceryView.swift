//
//  GroceryView.swift
//  RiseApp-SwiftUI
//
//  Created by Andrea Cascella on 11/04/2020.
//  Copyright © 2020 Simone Formisano. All rights reserved.
//

import SwiftUI

struct GroceryView: View {
    var body: some View {
        NavigationView {
            ZStack {
                VStack {
                    NavigationLink(destination: GrocerySelectedItem()) {
                        Image("mockup-3")
                            .resizable()
                            .scaledToFit()
                    }
                    .buttonStyle(PlainButtonStyle())
                    Spacer()
                }
            }
            .navigationBarItems (
                leading:
                Text("Templates")
                    .foregroundColor(Color(RiseColor(color: "front")!.color)),
                trailing:
                Image(systemName: "plus")
                    .foregroundColor(Color(RiseColor(color: "front")!.color))
            )
            .navigationBarTitle("Grocery", displayMode: .inline)
        }
    }
}

struct GroceryView_Previews: PreviewProvider {
    static var previews: some View {
        GroceryView()
    }
}
