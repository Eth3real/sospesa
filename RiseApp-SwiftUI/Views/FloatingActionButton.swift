//
//  FloatingActionButton.swift
//  RiseApp-SwiftUI
//
//  Created by Simone Formisano on 15/04/2020.
//  Copyright © 2020 Simone Formisano. All rights reserved.
//

import SwiftUI

struct FloatingActionButton: View {
    let buttonImage: Image
    var body: some View {
        Circle()
            .fill(Color(RiseColor.init(color: "front")!.color))
            .frame(width: 70, height: 70)
            .shadow(radius: 6, x: 3, y: 3)
            .overlay(
                buttonImage
                    .font(.title)
                    .foregroundColor(Color.white)
                    .frame(width: 70, height: 70)
                ,alignment: .bottomTrailing
            )
            .padding()
    }
}

struct FloatingActionButton_Previews: PreviewProvider {
    static var previews: some View {
        FloatingActionButton(buttonImage: Image(uiImage:UIImage(named: "Bag")!))
    }
}
