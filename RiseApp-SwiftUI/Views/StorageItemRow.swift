//
//  StorageItemRow.swift
//  RiseApp-SwiftUI
//
//  Created by Andrea Cascella on 11/04/2020.
//  Copyright © 2020 Simone Formisano. All rights reserved.
//

import SwiftUI

struct StorageItemRow: View {
    @State var quantity: Int
    var color = Color(RiseColor(color: "front")!.color)
    var item: Item
    
    var body: some View {
        ZStack{
            HStack{
                CircularItemImage(proPic: item.image, size: 100)
                    
                VStack(alignment: .leading){
                    Text(item.name)
                    HStack{
                        Image("label")
                            .resizable()
                            .frame(width: 24, height: 24)
                            .aspectRatio(1,contentMode: .fit )
                        Text(item.categories[0])
                    }
                    .foregroundColor(color)
                }
                Spacer()
                Image("minusButton")
                    .onTapGesture {
                        if self.quantity > 0 {
                            self.quantity -= 1
                        }
                    }
                Text("\(quantity)")
                    .foregroundColor(self.quantity == 0 ? Color.red : Color.black)
                    .padding(5)
                Image("plusButton").onTapGesture {
                    self.quantity += 1
                }
                
            }
        }
        .padding()
        .frame(minWidth: 300)
    }
}

struct StorageItemRow_Previews: PreviewProvider {
    static var previews: some View {
        StorageItemRow(quantity: 4, item: Item(id: UUID(), image: Image(uiImage:UIImage(named: "photoSanitizer")!),name: "Hand Sanitizer", categories: ["sanitizer"], shop: Shop(id: 0, image: UIImage.init(named: "p0")!, name: "Simply", address: "Via dell'Academy", inventory: [""])))
        .previewLayout(.fixed(width: 300, height: 100))
    }
}
