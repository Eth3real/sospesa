//
//  PersonRow.swift
//  customTableViewSwiftUITest
//
//  Created by Simone Formisano on 15/03/2020.
//  Copyright © 2020 Simone Formisano. All rights reserved.
//

import SwiftUI

struct ItemRow: View {
    var item: Item
    let profileImageSize: CGFloat = 100.0
    let shadowColor: Color = Color(red: 0, green: 0, blue: 0, opacity: 0.1)
    
    var body: some View {
        ZStack(alignment: .leading) {
            RoundedRectangle(cornerRadius: 20, style: .circular)
                .fill(Color.white)
                .shadow(color: shadowColor, radius: 12)
            HStack {
                CircularItemImage(proPic: item.image, size: profileImageSize)
                    .padding([.trailing])
                VStack(alignment: .leading) {
                    Text(item.name)
                        .font(.headline)
                        .bold()
                        .fontWeight(.medium)
                        .padding([.bottom], 7.0)
                    Text("\(item.shop.address)")
                        .font(.subheadline)
                        .foregroundColor(Color(RiseColor(color: "front")!.color))
                        .padding([.bottom])
                    VStack(alignment: .leading) {
                        Text("Still there?")
                        prograssBar()
                        ZStack{
                            Text("No")
                                .frame(maxWidth: .infinity, alignment: .leading)
                            Text("Yes")
                            .frame(maxWidth: .infinity, alignment: .trailing)
                        }
                    }
                    HStack {
                        Spacer()
                        Image("label")
                            .resizable()
                            .scaledToFit()
                        Text("\(item.categories[0])")
                            .font(.headline)
                            .foregroundColor(Color(RiseColor(color: "front")!.color))
                    }
                }
            }
            .frame(width: 300, height: 150, alignment: .leading)
            .padding(20)
        }
        .padding([.top], 15)
        .padding([.bottom], 10)
    }
}

struct prograssBar: View {
    var body: some View {
        ZStack(alignment: .leading) {
            RoundedRectangle(cornerRadius: 20, style: .circular)
                .fill(Color(RiseColor(color: "back")!.color))
            RoundedRectangle(cornerRadius: 20, style: .circular)
                .fill(Color(RiseColor(color: "front")!.color))
                .frame(height: 10)
                .frame(maxWidth: CGFloat.random(in: 1...200), alignment: .leading)
                .animation(.easeIn)
        }
        .frame(maxHeight: 15)
    }
}

struct PersonRow_Previews: PreviewProvider {
    static var previews: some View {
        Group {
            ItemRow(item: Item(id: UUID(), image: Image(uiImage:UIImage(named: "photoSanitizer")!),name: "lollo", categories: ["test category"], shop: Shop(id: 0, image: UIImage.init(named: "p0")!, name: "Simply", address: "Via dell'Academy", inventory: [""])))
                .previewLayout(.fixed(width: 300, height: 100))
        }
    }
}
