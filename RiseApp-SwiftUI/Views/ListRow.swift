//
//  ListRow.swift
//  RiseApp-SwiftUI
//
//  Created by Andrea Cascella on 11/04/2020.
//  Copyright © 2020 Simone Formisano. All rights reserved.
//

import SwiftUI

struct ListRow: View {
    let shadowColor: Color = Color(red: 0, green: 0, blue: 0, opacity: 0.1)
    var list: SpesaList
    @State var toggleList = false
    let animIn = Animation.easeIn(duration: 0.15).repeatCount(1)
    let animOut = Animation.easeOut(duration: 0.15).repeatCount(1)
    
    var body: some View {
        ZStack {
            RoundedRectangle(cornerRadius: 15, style: .circular)
            .fill(Color.white)
            .shadow(color: shadowColor, radius: 10)
            VStack(alignment: .leading){
                HStack{
                Text("Near")
                    Text("\(list.place):")
                        .foregroundColor(Color(RiseColor.init(color: "front")!.color))
                }
                HStack {
                    Text("List of \(list.quantity) items")
                        .font(.headline)
                        .fontWeight(.bold)
                    Image(systemName: "chevron.right")
                        .rotationEffect(toggleList ? .degrees(90) : .degrees(0))
                }
//                .animation(.easeInOut(duration: 0.3))
                .onTapGesture {
                    withAnimation {
                        self.toggleList.toggle()
                    }
                }
                if toggleList {
                    ForEach(list.elements, id: \.self){ element in
                        Text("• \(element)")
                            .font(.callout)
                    }
                    .animation(toggleList ? animIn : animOut)
                }
                VStack(alignment: .trailing) {
                    HStack {
                        Spacer()
                        Text("Total: ")
                        Text("\(list.total) €")
                            .foregroundColor(Color(RiseColor.init(color: "front")!.color))
                    }
                    .frame(width: 300)
                }
            }
            .frame(width: 300)
            .padding(20)
        }
        .frame(width: 300)
        .frame(height: 150)
        .padding(15)
    }
}

struct ListRow_Previews: PreviewProvider {
    static var previews: some View {
        ListRow(list: SpesaList(id: 1, elements: ["Meat", "Potatoes", "Pasta", "Flour"], place: "Naples", total: 25))
    }
}
