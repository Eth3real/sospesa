//
//  SoSpesaView.swift
//  RiseApp-SwiftUI
//
//  Created by Andrea Cascella on 11/04/2020.
//  Copyright © 2020 Simone Formisano. All rights reserved.
//

import SwiftUI

struct SoSpesaView: View {
    @State private var selectorIndex = 0
    @State private var numbers = ["Give","Ask","Pick Up"]
    
    var views = [SoSpesaQRView(), SoSpesaListView(), SoSpesaQRView()] as [Any]
    
    var body: some View {
        NavigationView {
            VStack {
                Picker("Numbers", selection: $selectorIndex) {
                    ForEach(0 ..< numbers.count) { index in
                        Text(self.numbers[index]).tag(index)
                    }
                }
                .pickerStyle(SegmentedPickerStyle())
                .padding()
                ZStack {
                    SoSpesaListView()
                        .opacity(selectorIndex != 0 ? 0 : 1)
                    SoSpesaAskView()
                        .opacity(selectorIndex != 1 ? 0 : 1)
                    SoSpesaQRView()
                        .opacity(selectorIndex != 2 ? 0 : 1)
                }
                Spacer()
            }
            .navigationBarTitle("SoSpesa", displayMode: .inline)
        }
        
    }
}

struct SoSpesaListView: View {
    @State private var lists: [SpesaList] = [
        SpesaList(id: 0, elements: ["Meat", "Flour", "Milk", "Cookies"], place: "Napoli", total: 15),
        SpesaList(id: 1, elements: ["Pasta", "Soap", "Honey"], place: "Roma", total: 8),
        SpesaList(id: 2, elements: ["Paper", "Rice", "Apples", "Bread", "Toothpaste"], place: "Milano", total: 20),
        SpesaList(id: 3, elements: ["Paper", "Rice", "Apples", "Bread", "Toothpaste"], place: "Milano", total: 20)
    ]
    
    var body: some View {
        List {
            ForEach(lists) { list in
                ListRow(list: list)
            }
            .listRowInsets(EdgeInsets(top: 16 , leading: 40, bottom: 15, trailing: 0))
        }
        .listStyle(PlainListStyle())
        .onAppear(perform: {
            UITableView.appearance().separatorColor = .clear
        })
    }
}

struct SoSpesaAskView: View {
    var body: some View {
        VStack {
            AskRow(label: "Use a list template")
            AskRow(label: "Create a new list")
        }
        .padding([.bottom])
    }
}

struct SoSpesaQRView: View {
    var body: some View {
        VStack{
            Image("websiteQRCode_noFrame")
                .resizable()
                .aspectRatio(1, contentMode: .fit)
            Text("Scan the QR to pick up your grocery!")
                .font(.system(size: 36))
                .multilineTextAlignment(.center)
                .foregroundColor(Color(RiseColor(color: "front")!.color))
            Spacer()
            HStack {
                Text("2/4 items have already been paid for by: ")
                + Text("Anonymous")
                    .foregroundColor(Color(RiseColor(color: "front")!.color))
            }
            .padding()
            Spacer()
        }
    }
}

struct AskRow: View {
    let shadowColor: Color = Color(red: 0, green: 0, blue: 0, opacity: 0.1)
    @State var label: String = ""
    
    var body: some View {
        ZStack {
            RoundedRectangle(cornerRadius: 15, style: .circular)
                .fill(Color.white)
                .shadow(color: shadowColor, radius: 10)
            HStack {
                Text(label)
                Spacer()
                Image(systemName: "chevron.right")
                    .foregroundColor(Color(RiseColor(color: "front")!.color))
            }
            .padding(40)
        }
        .frame(height: 150)
        .padding()
    }
}

struct SoSpesaView_Previews: PreviewProvider {
    static var previews: some View {
        SoSpesaView()
    }
}
