//
//  GrocerySelectedItem.swift
//  RiseApp-SwiftUI
//
//  Created by Simone Formisano on 15/04/2020.
//  Copyright © 2020 Simone Formisano. All rights reserved.
//

import SwiftUI

struct GrocerySelectedItem: View {
    @State var tapped = false

    var grocList = [
        Item(id: UUID(), image: Image(uiImage: UIImage(named: "photoSanitizer")!), name: "Hand Sanitizer 300ml", categories: [Category.detergent], shop: Shop(id: 0, image: UIImage.init(named: "p0")!, name: "Simply", address: "Parco della Vittoria, 21", inventory: [""])),
        Item(id: UUID(), image: Image(uiImage: UIImage(named: "paper")!), name: "Paper IGP", categories: [Category.paper], shop: Shop(id: 0, image: UIImage.init(named: "p0")!, name: "Primark", address: "Oxford Street", inventory: [""])),
        Item(id: UUID(), image: Image(uiImage: UIImage(named: "paper")!), name: "Meat balls", categories: [Category.food], shop: Shop(id: 0, image: UIImage.init(named: "p0")!, name: "Primark", address: "Oxford Street", inventory: [""]))
    ]
    var body: some View {
        ZStack(alignment: .bottomTrailing) {
            List(grocList) { groc in
                grocItem(groc: groc)
            }
            .offset(x: -15)
            Rectangle()
                .opacity(tapped ? 0.5 : 0)
                .animation(.easeInOut(duration: 0.2))
                .onTapGesture {
                    self.tapped.toggle()
                }
            VStack(alignment: .trailing) {
                ZStack {
                    RoundedRectangle(cornerRadius: 15)
                        .fill(Color.white)
                        .frame(width: 200, height: 110)
                        .offset(x: -10, y: 10)
                        .padding()
                    VStack {
                        Text("Ask SoSpesa!")
                        Divider()
                        Text("Make tamplate")
                    }.offset(x: -10, y: 10)
                }
                .frame(width: 200, height: 110)
                .opacity(tapped ? 1 : 0)
                .animation(.easeInOut(duration: 0.1))
                Button(action: {
                    self.tapped.toggle()
                }) {
                    FloatingActionButton(buttonImage: tapped ? Image(systemName: "xmark") : Image(uiImage:UIImage(named: "Bag")!))
                }
            }
        }
    }
}

struct grocItem: View {
    let groc: Item
    @State var selected = false
    
    var body: some View {
        VStack {
            HStack {
                ZStack {
                    Circle()
                        .frame(width: 50)
                        .onTapGesture {
                            self.selected.toggle()
                    }
                    .overlay(
                        Circle()
                            .stroke(Color(RiseColor(color: "front")!.color),lineWidth: 2)
                    )
                    .foregroundColor(self.selected ? Color(RiseColor(color: "front")!.color) : Color.white)
                    .padding()
                    
                    Image(systemName: "checkmark")
                        .foregroundColor(.white)
                }
                    
                VStack(alignment: .leading) {
                    Text(groc.name)
                    
                    HStack {
                        Image(uiImage: UIImage(named: "label")!)
                            .resizable()
                            .frame(width: 20, height: 20)
                        Text(groc.categories[0])
                    }
                }
            }
            .frame(height: 101)
        }
    }
}

struct GrocerySelectedItem_Previews: PreviewProvider {
    static var previews: some View {
        GrocerySelectedItem()
    }
}
