//
//  MapView.swift
//  RiseApp-SwiftUI
//
//  Created by Andrea Cascella on 11/04/2020.
//  Copyright © 2020 Simone Formisano. All rights reserved.
//

import CoreLocation
import MapKit
import SwiftUI

struct MapView: UIViewRepresentable {
    class Coordinator: NSObject, MKMapViewDelegate{
        var parent: MapView
        
        init(_ parent: MapView) {
            self.parent = parent
        }
    }
    
    let locationManager = CLLocationManager()
    
    func makeCoordinator() -> Coordinator {
        Coordinator(self)
    }
    
    func makeUIView(context: Context) -> MKMapView {
        let mapView = MKMapView()
        mapView.delegate = context.coordinator
        
//        let annotation = MKPointAnnotation()
//        annotation.title = "Nome"
//        annotation.subtitle = "Indirizzo"
//        annotation.coordinate = randomLocations.getMockLocationsFor(/*here*/)
//
        return mapView
    }
    
    func updateUIView(_ view: MKMapView, context: Context) {
        
    }
}

struct MapView_Previews: PreviewProvider {
    static var previews: some View {
        MapView()
    }
}
