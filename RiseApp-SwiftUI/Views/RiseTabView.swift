//
//  TabView.swift
//  RiseApp-SwiftUI
//
//  Created by Andrea Cascella on 10/04/2020.
//  Copyright © 2020 Simone Formisano. All rights reserved.
//

import SwiftUI

struct RiseTabView: View {
    let color = RiseColor(color: "front")!.color
    
    var body: some View {
        TabView {
            MainView()
                .tabItem {
                    Image(systemName: "list.bullet")
                    Text("Items")
                }
            ShopsView()
                .tabItem {
                    Image(systemName: "cart")
                    Text("Shops")
                }
            StorageView()
                .tabItem {
                    Image(systemName: "cube.box")
                    Text("Storage")
                }
            GroceryView()
                .tabItem {
                    Image(systemName: "doc.plaintext")
                    Text("Grocery")
                }
            SoSpesaView()
                .tabItem {
                    Image(systemName: "dollarsign.circle")
                    Text("SOSpesa")
                }
        }
        .accentColor(Color(RiseColor(color: "front")!.color))
    }
}

struct TabView_Previews: PreviewProvider {
    static var previews: some View {
        RiseTabView()
    }
}
